package model;

import utils.UTIL;

public class SimulationSettings {
    Double dataFrom;
    Double dataTo;
    Integer stepsNumber;
    UTIL.SimulationType simulationType;

    public SimulationSettings(){}

    public SimulationSettings(Double dataFrom, Double dataTo, Integer stepsNumber, UTIL.SimulationType simulationType) {
        this.dataFrom = dataFrom;
        this.dataTo = dataTo;
        this.stepsNumber = stepsNumber;
        this.simulationType = simulationType;
    }

    public Double getDataFrom() {
        return dataFrom;
    }

    public void setDataFrom(Double dataFrom) {
        this.dataFrom = dataFrom;
    }

    public Double getDataTo() {
        return dataTo;
    }

    public void setDataTo(Double dataTo) {
        this.dataTo = dataTo;
    }

    public Integer getStepsNumber() {
        return stepsNumber;
    }

    public void setStepsNumber(Integer stepsNumber) {
        this.stepsNumber = stepsNumber;
    }

    public UTIL.SimulationType getSimulationType() {
        return simulationType;
    }

    public void setSimulationType(UTIL.SimulationType simulationType) {
        this.simulationType = simulationType;
    }

    @Override
    public String toString() {
        return "SimulationSettings{" +
                "dataFrom=" + dataFrom +
                ", dataTo=" + dataTo +
                ", stepsNumber=" + stepsNumber +
                ", simulationType=" + simulationType +
                '}';
    }
}
