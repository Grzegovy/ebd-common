package model;

import java.util.Map;

public class CalculationResult {
    private Map<Double, Double> resultMap;
    private String layerName;
    private Double from, to;
    private Integer steps;
    private String seriesName;

    public CalculationResult(){}

    public CalculationResult(Map<Double, Double> resultMap, String layerName, Double from, Double to, Integer steps, String seriesName) {
        this.resultMap = resultMap;
        this.layerName = layerName;
        this.from = from;
        this.to = to;
        this.steps = steps;
        this.seriesName = seriesName;
    }

    public Map<Double, Double> getResultMap() {
        return resultMap;
    }

    public void setResultMap(Map<Double, Double> resultMap) {
        this.resultMap = resultMap;
    }

    public String getLayerName() {
        return layerName;
    }

    public void setLayerName(String layerName) {
        this.layerName = layerName;
    }

    public Double getFrom() {
        return from;
    }

    public void setFrom(Double from) {
        this.from = from;
    }

    public Double getTo() {
        return to;
    }

    public void setTo(Double to) {
        this.to = to;
    }

    public Integer getSteps() {
        return steps;
    }

    public void setSteps(Integer steps) {
        this.steps = steps;
    }

    public String getSeriesName() {
        return seriesName;
    }

    public void setSeriesName(String seriesName) {
        this.seriesName = seriesName;
    }

    @Override
    public String toString() {
        return "CalculationResult{" +
                "resultMap=" + resultMap +
                ", layerName='" + layerName + '\'' +
                ", from=" + from +
                ", to=" + to +
                ", steps=" + steps +
                ", seriesName='" + seriesName + '\'' +
                '}';
    }
}
