package engine;

import model.Cache;
import model.SimulationSettings;

import java.util.List;

public interface DiagramEngine {

    void refreshProgressBar(Double progressValue, String actualStatus);

    boolean checkDataInRange(SimulationSettings ss);

    void clearCache();
    void dumpCacheToFile();

    void recalculate();
    void recalculate(Boolean clearCache);

    List<Cache.CalculationResult> getEnergyCalculationResultList(SimulationSettings simulationSettings) throws DiagramEngineController.DiagramEngineControllerException;
    List<Cache.CalculationResult> getPotentialCalculationResultList(SimulationSettings simulationSettings) throws DiagramEngineController.DiagramEngineControllerException;
    List<Cache.CalculationResult> getElectricFieldCalculationResultList(SimulationSettings simulationSettings) throws DiagramEngineController.DiagramEngineControllerException;
    List<Cache.CalculationResult> getChargeDensityCalculationResultList(SimulationSettings simulationSettings) throws DiagramEngineController.DiagramEngineControllerException;
}
