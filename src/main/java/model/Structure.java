package model;

import java.util.ArrayList;
import java.util.List;

public class Structure {
    private String name;
    private String description;
    private ArrayList<Layer> layers;
    private Double  totalThickness;

    public Structure() {}

    public void addLayer(Layer layer) {
        layers.add(layer);
    }

    public void deleteLayer(Integer index) {
        layers.remove(index);
    }

    public void moveDownLayer(Integer layerIndex) {
        if(layerIndex < layers.size() - 1) {
            Layer old = layers.get(layerIndex);
            layers.set(layerIndex, layers.get(layerIndex + 1));
            layers.set(layerIndex + 1, old);
        }
    }

    public void moveUpLayer(Integer layerIndex) {
        if(layerIndex > 0) {
            Layer old = layers.get(layerIndex);
            layers.set(layerIndex, layers.get(layerIndex - 1));
            layers.set(layerIndex - 1, old);
        }
    }

    public void recalculateTotalThickness() {
        Double thickness = 0.0;
        for(Layer layer : this.layers) {
            thickness += layer.getThickness();
        }
        this.totalThickness = thickness;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Layer> getLayers() {
        return layers;
    }

    public void setLayers(ArrayList<Layer> layers) {
        this.layers = layers;
    }

    public void setTotalThickness(Double totalThickness) {
        this.totalThickness = totalThickness;
    }

    public Double getTotalThickness() {
        recalculateTotalThickness();
        return totalThickness;
    }

    @Override
    public String toString() {
        return "Structure{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", layers=" + layers +
                ", totalThickness=" + totalThickness +
                '}';
    }
}
