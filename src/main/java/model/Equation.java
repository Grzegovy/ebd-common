package model;

import java.util.List;

public class Equation {
    private String name;
    private String equation;
    private List<String> requiredTokes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEquation() {
        return equation;
    }

    public void setEquation(String equation) {
        this.equation = equation;
    }

    public List<String> getRequiredTokes() {
        return requiredTokes;
    }

    public void setRequiredTokes(List<String> requiredTokes) {
        this.requiredTokes = requiredTokes;
    }

    @Override
    public String toString() {
        return "Equation{" +
                "name='" + name + '\'' +
                ", equation='" + equation + '\'' +
                ", requiredTokes=" + requiredTokes +
                '}';
    }
}
