package engine;

import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import model.Cache;
import model.ModelConfiguration;
import model.SimulationSettings;
import model.Structure;
import utils.UTIL_DataTransfer;

import java.util.List;

public class DiagramEngineController implements DiagramEngine {

    public class DiagramEngineControllerException extends Exception {
        public DiagramEngineControllerException(String message) {
            super(message);
        }
    }

    private ProgressBar progressBar;
    private Label progressBarLabel;

    protected ModelConfiguration model;
    protected Structure structure;
    protected Cache cache;

    public DiagramEngineController(){
        this.cache = new Cache();
    };

    public DiagramEngineController getControllerInstance() {
        System.out.println("Returing instance of DiagramEngineController: Main Application");
        return this;
    }

    public void SetControllerParameters(String modelJson, String structureJson) throws UTIL_DataTransfer.DataTransferException {
        try {
            ModelConfiguration model = (ModelConfiguration) UTIL_DataTransfer.getObjectInstanceFromJson(modelJson, ModelConfiguration.class);
            Structure structure = (Structure) UTIL_DataTransfer.getObjectInstanceFromJson(structureJson, Structure.class);
            this.model = model;
            this.structure = structure;
        } catch (Exception e) {
            System.out.println("Exception in: DiagramEngineController");
            e.getMessage();
            e.getStackTrace();
            throw new UTIL_DataTransfer.DataTransferException(e.getMessage());
        }
    }

//    public void SetControllerParameters(ModelConfiguration model, Structure structure) {
//        this.model = model;
//        this.structure = structure;
//    }

    public void enableProgressBar(ProgressBar progressBar, Label progressBarLabel) {
        this.progressBar = progressBar;
        this.progressBarLabel = progressBarLabel;
    }

    @Override
    public void refreshProgressBar(Double progressValue, String actualStatus) {
        Platform.runLater(() -> {
            this.progressBar.setProgress(progressValue);
            this.progressBarLabel.setText(actualStatus);
        });
    }

    @Override
    public boolean checkDataInRange(SimulationSettings ss) {
        Double cacheMin = this.cache.getMinimumSimulationRange(ss.getSimulationType());
        Double cacheMax = this.cache.getMaximumSimulationRange(ss.getSimulationType());

        if(ss.getDataFrom() >= cacheMin && ss.getDataTo() <= cacheMax) {
            return true;
        } else {
            return false;
        }

    }

    @Override
    public void clearCache() {
        this.cache = new Cache();
    }

    @Override
    public void dumpCacheToFile() {
        this.cache.dumpStandardCacheAsJson();
    }

    @Override
    public void recalculate() {

    }

    @Override
    public void recalculate(Boolean clearCache) {
        if(clearCache) clearCache();
        recalculate();
    }

    public String getEnergyCalculationResultList(String simulationSettingsJson) throws UTIL_DataTransfer.DataTransferException, DiagramEngineControllerException {
        try {
            SimulationSettings ss = (SimulationSettings) UTIL_DataTransfer.getObjectInstanceFromJson(simulationSettingsJson, SimulationSettings.class);
            return UTIL_DataTransfer.serializeObject(getEnergyCalculationResultList(ss));
        } catch (Exception e) {
            return e.getMessage() + e.getStackTrace();
        }
    }

    public String getPotentialCalculationResultList(String simulationSettingsJson) throws UTIL_DataTransfer.DataTransferException, DiagramEngineControllerException {
        SimulationSettings ss = (SimulationSettings) UTIL_DataTransfer.getObjectInstanceFromJson(simulationSettingsJson, SimulationSettings.class);
        return UTIL_DataTransfer.serializeObject(getPotentialCalculationResultList(ss));
    }

    public String getElectricFieldCalculationResultList(String simulationSettingsJson) throws UTIL_DataTransfer.DataTransferException, DiagramEngineControllerException {
        SimulationSettings ss = (SimulationSettings) UTIL_DataTransfer.getObjectInstanceFromJson(simulationSettingsJson, SimulationSettings.class);
        return UTIL_DataTransfer.serializeObject(getElectricFieldCalculationResultList(ss));
    }

    public String getChargeDensityCalculationResultList(String simulationSettingsJson) throws UTIL_DataTransfer.DataTransferException, DiagramEngineControllerException {
        SimulationSettings ss = (SimulationSettings) UTIL_DataTransfer.getObjectInstanceFromJson(simulationSettingsJson, SimulationSettings.class);
        return UTIL_DataTransfer.serializeObject(getChargeDensityCalculationResultList(ss));
    }

    public List<Cache.CalculationResult> getEnergyCalculationResultList(SimulationSettings simulationSettings) throws DiagramEngineControllerException {
        throw new DiagramEngineControllerException("Simulation Type not supported in selected model!");
    }

    public List<Cache.CalculationResult> getPotentialCalculationResultList(SimulationSettings simulationSettings) throws DiagramEngineControllerException {
        throw new DiagramEngineControllerException("Simulation Type not supported in selected model!");
    }

    public List<Cache.CalculationResult> getElectricFieldCalculationResultList(SimulationSettings simulationSettings) throws DiagramEngineControllerException {
        throw new DiagramEngineControllerException("Simulation Type not supported in selected model!");
    }

    public List<Cache.CalculationResult> getChargeDensityCalculationResultList(SimulationSettings simulationSettings) throws DiagramEngineControllerException {
        throw new DiagramEngineControllerException("Simulation Type not supported in selected model!");
    }
}
