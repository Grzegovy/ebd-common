package model;

import utils.UTIL;

public class Layer {
    private String name;
    private String color;
    private String description;
    private Material material;
    private Double thickness;

    public Layer() {}

    public Layer(Material material) {
        this.material = material;
        material.putParameter("{d}", new Parameter("{d}", "d", "Thickness", "nm", "0.0",  false));
    }

    public Double getParameterValue(String name) {
        if(material.getParameters().containsKey(name)) {
            return material.getParameters().get(name).getValue();
        } else {
            return null;
        }
    }

    public Boolean isBetween(Double from, Double to, Double startingPoint) {
        Double layerBegin = startingPoint;
        Double layerEnd = startingPoint + this.thickness;
        System.out.println("layerBegin: " + layerBegin + " layerEnd: " + layerEnd + " startingPoint: " + startingPoint);
        if(layerBegin >= to ) {
            System.out.println("Layer is not in range: LEFT reason.");
            return false;
        } else if(layerEnd <= from) {
            System.out.println("Layer is not in range: RIGHT reason.");
            return false;
        } else {
            return true;
        }
    }

    public UTIL.CommonPartType getCommonPartType(Double from, Double to, Double startingPoint) {
        Double layerBegin = startingPoint;
        Double layerEnd = startingPoint + this.thickness;

        if(from < layerBegin && to > layerBegin && to < layerEnd) {
            System.out.println("Common part: LEFT");
            return UTIL.CommonPartType.LEFT;
        } else if(from > layerBegin && from < layerEnd && to > layerEnd) {
            System.out.println("Common part: RIGHT");
            return UTIL.CommonPartType.RIGHT;
        } else if(from >= layerBegin && from <= layerEnd && to >= layerBegin && to <= layerEnd && from < to) {
            System.out.println("Common part: INSIDE");
            // TODO Exception here if from > to ?
            return UTIL.CommonPartType.INSIDE;
        } else if(from <= layerBegin && to >= layerEnd) {
            System.out.println("Common part: OUTSIDE");
            return UTIL.CommonPartType.OUTSIDE;
        } else {
            System.out.println("Common part: ERROR");
            return UTIL.CommonPartType.ERROR;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addParameter(Parameter p) {
        material.putParameter(p.getToken(), p);
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public Double getThickness() {
        return thickness;
    }

    public void setThickness(Double thickness) {
        this.thickness = thickness;
    }

    @Override
    public String toString() {
        return "Layer{" +
                "name='" + name + '\'' +
                ", color='" + color + '\'' +
                ", description='" + description + '\'' +
                ", material=" + material +
                ", thickness=" + thickness +
                '}';
    }
}
