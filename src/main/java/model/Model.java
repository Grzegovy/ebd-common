package model;


import java.util.HashMap;

public class Model {
    private String name;
    private String author;
    private String version;
    private String description;
    private String modelPath;
    private String rootPath;

    private HashMap<String, Equation> equationMap;
    private HashMap<String, Constant> constantsMap;
    private HashMap<String, Parameter> parametersMap;
    private HashMap<String, Material> materialsMap;

    public Model() {}

    public Model(ModelConfiguration mc) {
        this.name = mc.getName();
        this.author = mc.getAuthor();
        this.version = mc.getVersion();
        this.description = mc.getDescription();
        this.rootPath = mc.getRootPath();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getModelPath() {
        return modelPath;
    }

    public void setModelPath(String modelPath) {
        this.modelPath = modelPath;
    }

    public String getRootPath() {
        return rootPath;
    }

    public void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }

    public HashMap<String, Equation> getEquationMap() {
        return equationMap;
    }

    public void setEquationMap(HashMap<String, Equation> equationMap) {
        this.equationMap = equationMap;
    }

    public HashMap<String, Constant> getConstantsMap() {
        return constantsMap;
    }

    public void setConstantsMap(HashMap<String, Constant> constantsMap) {
        this.constantsMap = constantsMap;
    }

    public HashMap<String, Parameter> getParametersMap() {
        return parametersMap;
    }

    public void setParametersMap(HashMap<String, Parameter> parametersMap) {
        this.parametersMap = parametersMap;
    }

    public HashMap<String, Material> getMaterialsMap() {
        return materialsMap;
    }

    public void setMaterialsMap(HashMap<String, Material> materialsMap) {
        this.materialsMap = materialsMap;
    }
}
