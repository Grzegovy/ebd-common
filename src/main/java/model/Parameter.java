package model;

public class Parameter {
    private String token;
    private String symbol;
    private String name;
    private String unit;
    private String valueString;
    private double value;
    private Boolean editable;

    public Parameter(){}

    public Parameter(String token, String symbol, String name, String unit) {
        this.token = token;
        this.symbol = symbol;
        this.name = name;
        this.unit = unit;
    }

    public Parameter(String token, String symbol, String name, String unit, String valueString, Boolean editable) {
        this.token = token;
        this.symbol = symbol;
        this.name = name;
        this.unit = unit;
        this.valueString = valueString;
        this.editable = editable;

        if(valueString != null && !valueString.equals("")) {
            this.value = Double.valueOf(valueString);
        }
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getValueString() {
        return valueString;
    }

    public void setValueString(String valueString) {
        this.valueString = valueString;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public Boolean getEditable() {
        return editable;
    }

    public void setEditable(Boolean editable) {
        this.editable = editable;
    }

    @Override
    public String toString() {
        return "Parameter{" +
                "token='" + token + '\'' +
                ", symbol='" + symbol + '\'' +
                ", name='" + name + '\'' +
                ", unit='" + unit + '\'' +
                ", valueString='" + valueString + '\'' +
                ", value=" + value +
                ", editable=" + editable +
                '}';
    }
}

