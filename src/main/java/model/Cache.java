package model;

import utils.UTIL;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Cache {
    Map<UTIL.SimulationType, Map<Double, Double>> standardCacheMap;
    Map<Object, Object> customCacheMap;

    public Cache() {
        standardCacheMap = new HashMap<UTIL.SimulationType, Map<Double, Double>>();
        standardCacheMap.put(UTIL.SimulationType.ENERGY, new HashMap<Double, Double>());
        standardCacheMap.put(UTIL.SimulationType.POTENTIAL, new HashMap<Double, Double>());
        standardCacheMap.put(UTIL.SimulationType.CHARGE_DENSITY, new HashMap<Double, Double>());
        standardCacheMap.put(UTIL.SimulationType.ELECTRIC_FIELD, new HashMap<Double, Double>());

        customCacheMap = new HashMap<Object, Object>();
    }

    public boolean dumpStandardCacheAsJson() {
        return false;
    }

    public Map<UTIL.SimulationType, Map<Double, Double>> getStandardCacheMap() {
        return standardCacheMap;
    }

    public void setStandardCacheMap(Map<UTIL.SimulationType, Map<Double, Double>> standardCacheMap) {
        this.standardCacheMap = standardCacheMap;
    }

    public Map<Object, Object> getCustomCacheMap() {
        return customCacheMap;
    }

    public void setCustomCacheMap(Map<Object, Object> customCacheMap) {
        this.customCacheMap = customCacheMap;
    }

    public Double getMinimumSimulationRange(UTIL.SimulationType st) {
        Set<Double> rangeList = this.standardCacheMap.get(st).keySet();
        return Collections.min(rangeList);
    }

    public Double getMaximumSimulationRange(UTIL.SimulationType st) {
        Set<Double> rangeList = this.standardCacheMap.get(st).keySet();
        return Collections.max(rangeList);
    }

    public static class CalculationResult {
        private Map<Double, Double> resultMap;
        private String layerName;
        private Double from, to;
        private Integer steps;
        private String seriesName;

        public CalculationResult(Map<Double, Double> resultMap, String layerName, Double from, Double to, Integer steps, String seriesName) {
            this.resultMap = resultMap;
            this.layerName = layerName;
            this.from = from;
            this.to = to;
            this.steps = steps;
            this.seriesName = seriesName;
        }

        public Map<Double, Double> getResultMap() {
            return resultMap;
        }

        public void setResultMap(Map<Double, Double> resultMap) {
            this.resultMap = resultMap;
        }

        public String getLayerName() {
            return layerName;
        }

        public void setLayerName(String layerName) {
            this.layerName = layerName;
        }

        public Double getFrom() {
            return from;
        }

        public void setFrom(Double from) {
            this.from = from;
        }

        public Double getTo() {
            return to;
        }

        public void setTo(Double to) {
            this.to = to;
        }

        public Integer getSteps() {
            return steps;
        }

        public void setSteps(Integer steps) {
            this.steps = steps;
        }

        public String getSeriesName() {
            return seriesName;
        }

        public void setSeriesName(String seriesName) {
            this.seriesName = seriesName;
        }

        @Override
        public String toString() {
            return "CalculationResult{" +
                    "resultMap=" + resultMap +
                    ", layerName='" + layerName + '\'' +
                    ", from=" + from +
                    ", to=" + to +
                    ", steps=" + steps +
                    ", seriesName='" + seriesName + '\'' +
                    '}';
        }
    }
}
