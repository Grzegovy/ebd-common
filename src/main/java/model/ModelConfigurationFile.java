package model;

import utils.UTIL;

public class ModelConfigurationFile {
    String path;
    UTIL.ModelConfigurationFileType type;
    String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public UTIL.ModelConfigurationFileType getType() {
        return type;
    }

    public void setType(UTIL.ModelConfigurationFileType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "ModelConfigurationFile{" +
                "path='" + path + '\'' +
                ", type=" + type +
                ", description='" + description + '\'' +
                '}';
    }
}
