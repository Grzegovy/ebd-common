package utils;

import com.fasterxml.jackson.databind.ObjectMapper;

public class UTIL_DataTransfer {

    public static class DataTransferException extends Exception {
        public DataTransferException(String message) {
            super(message);
        }
    }

    public static Object getObjectInstanceFromJson(String jsonString, Class<?> type) throws DataTransferException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(jsonString, type);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println(e.getStackTrace());
            throw new DataTransferException(e.getMessage());
        }
    }

    public static String serializeObject(Object object) throws DataTransferException {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            String structureString = (String)objectMapper.writeValueAsString(object);
            return structureString;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println(e.getStackTrace());
            throw new DataTransferException(e.getMessage());
        }
    }

}
