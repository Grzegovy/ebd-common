package model;

import java.util.List;

public class ModelConfiguration {
    private String name;
    private String author;
    private String version;
    private String description;
    private String rootPath;
    private String jarPath;
    private List<ModelConfigurationFile> files;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRootPath() {
        return rootPath;
    }

    public void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }

    public String getJarPath() {
        return jarPath;
    }

    public void setJarPath(String jarPath) {
        this.jarPath = jarPath;
    }

    public List<ModelConfigurationFile> getFiles() {
        return files;
    }

    public void setFiles(List<ModelConfigurationFile> files) {
        this.files = files;
    }

    @Override
    public String toString() {
        return "ModelConfiguration{" +
                "name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", version='" + version + '\'' +
                ", description='" + description + '\'' +
                ", rootPath='" + rootPath + '\'' +
                ", jarPath='" + jarPath + '\'' +
                ", files=" + files +
                '}';
    }
}

